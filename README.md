# ZOO_MES Secure Entertainment System
### 2019 MITRE Collegiate eCTF 
***
***
This repository contains the University of Massachusetts Amherst a/k/a ZOO_MES MITRE Entertainment System source code.

This system was based off of [the supplied sample implementation](https://github.com/mitre-cyber-academy/2019-ectf-insecure-example).

Our implementation is outlined here, but detailed comments can be found in the source code.

## Overview

We use a sign-encrypt-MAC flow to protect game integrity and confidentiality. Libsodium’s encryption uses a encrypt then MAC flow to provide integrity and avoid violation of the Cryptographic Doom Principle. We sign the plaintext first to also ensure game integrity even if the encryption key is compromised.

### Tools: 

##### Libsodium [[Documentation]](https://pynacl.readthedocs.io/en/stable/)

Libsodium is an open-source cryptography library 
We used Libsodium in order to construct our integrity and confidentiality measures related to game binaries, versioning, and user accounts.

##### PyNaCL [[Documentation]](https://download.libsodium.org/doc/)

Python bindings for the Libsodium library.

## Design Implementation Details

### provisionSystem.py

During the system provisioning process, `provisionSystem.py` transforms the `Users.txt` and `default_games.txt` file into C header files, which are included in the MITRE Entertainment SHell.
This provides access to the users and pins which are allowed to login to the system, as well as default game requirements.

This script generates the following: an asymmetric key pair for Libsodium’s implementation of the Ed25519 digital signature system, which are used to generate signatures for the game and verify them before the games are played, a symmetric key for Libsodium’s crypto_secretbox and PyNaCl’s SecretBox APIs, which is used to encrypt the game after the signature has been generated and prepended to its header, and a symmetric key for Libsodium’s Argon2id based crypto_pwhash API, corresponding to PyNaCl’s nacl.pwhash, which is used to hash each user account PIN. 

This script also generates a `bif` file to specify boot information, as well as an empty `FactorySecrets` file.

The `provisionSystem.py` script builds U-Boot, the Kernel, the Device Tree, and the FileSystem (INITRAMFS).
Each of these components is described in greater detail below.

In order to boot the kernel from memory at 0x10000000 we specify where to load the image in the SystemImage.bif. This configuration is set using the load option.

 `[load 0x10000000] path/to/image.ub`.

This line tells the board to load `image.ub` into ram at location `0x10000000` when the board boots.

### provisionGames.py

During the game provisioning process, signing keys are used to generate signatures for respective games which are then prepended to the game headers. The signatures are verified, then `provisionGames.py` takes each entry in `Games.txt` and creates a binary with the following format:

`<gameName>-v<gameVersion>`

	version:<gameVersion>\nname:<gameName>\nusers:<user1> <user2>\n<gamebinary>

For example, for a 2048 game, the file would look as follows
`2048-v1.0`

	version:1.0\nname:2048users:demo\n\x90\x23\x23...

Afterwards, games are encrypted through the SecretBox API, using the corresponding secret.

### packageSystem.py

During the deployment process, `packageSystem.py` will open the bif file generated in the System Provisioning phase and use it while calling the `bootgen` command.
This will build the `MES.bin` binary.

### deploySystem.py

During the deployment process, `deploySystem.py` will mount the SD card and copy the appropriate boot files onto the SD card.
Finally, the script copies any provisioned games onto the SD card.

### PetaLinux Game Loader

In order to play a game, PetaLinux loads the game binary from a specified location in flash.
There are 2 main files that contain the code for this process.
One is the source code for the C program that loads the game from RAM and writes it to flash.
The second is the startup script that prevents the user from accessing the PetaLinux shell.
You may want to change this in your final design.
As it is now, the game loader is functional, but simple.

#### main.c

Location: `/ectf-collegiate/MES/Arty-Z7-10/project-spec/meta-user/recipes-apps/mesh-game-loader/files/main.c`

This file loads the game from flash at location `0x1FC00000`. It first reads the size of the game as a 4 byte integer from `0x1FC00000` then reads this length of bytes at location `0x1FC00040`. It then parses out the first 3 metadata lines appended to the beginning of the game binary and writes the remaining bytes to a file. This creates a file in PetaLinux that is an executable binary.

#### startup.sh
Location: `/ectf-collegiate/MES/Arty-Z7-10/project-spec/meta-user/recipes-apps/mesh-game-loader/files/startup.sh`

This file is specified as a startup script in the `mesh-game-loader/mesh-game-loader.bb` file, therefore it runs when PetaLinux boots up. It then creates a file for the game to be written, calls mesh-game-loader to write the game binary into the file, gives this file executable permissions, configures the serial device, and then runs the game. After the game executes, the startup script triggers a restart, preventing the user from falling through to the bash prompt.

It is important to note that configuring the serial device must be done BEFORE playing the game. As the rules specify, you must have a serial device accessible to the game that is configured at a baud rate of 115200. This is used for debugging from within the game and for automated testing. If the baud rate is not set to 115200, we will not be able to see the serial output from the game and you will not have a valid design.

### U-Boot and MESH Details

This design implements MITRE Entertainment Shell in the Second Stage Bootloader (U-Boot). Some features of U-Boot have been disabled, including the ethernet & network devices.
The typical U-Boot shell has been replaced with a CLI which supports the command described in the rules and requirements documents.
The details of how each command is implemented are described below to give you an idea of how to use the features of the Arty-Z7.

#### Game Install Table

Installed games and the associated user information are tracked in flash memory.
This is done via a table in which each row contains a flag and depending on the value of the flag, a game name, and the user that the game is installed for.
The row is a struct defined in `include/mesh.h`.
There is a checksum field to the end of the game table flash struct. Libsodium uses SHA-3 finalist Blake2b, which does not use a Merkle-Damgård construction that would be vulnerable to length extension attacks. 

The flag can have 3 values.

`0x00` - A game was installed in this row but it is now uninstalled.
`0x01` - A game is currently installed.
`0xFF` - You are at the end of the table and there are no guarantees about the data found after this location.

The game table must be valid for the commands below to work.
A valid table is defined as one that starts at flash address `0x044`, is made up of a contiguous series of row structs (`games_tbl_row`), in which the last row has the flag `0xFF`.
This is achieved by using a sentinel to determine if the table is initialized.
This sentinel is a random 4 byte value written at flash address `0x40`.
If the sentinel value is found at `0x40` then the table is initialized. If it is not, then MESH writes the sentinel at `0x40` and writes the end of table flag at `0x44`.
The install and uninstall commands are required maintain this invariant whenever they operate on the table.

#### help

Usage: `help`

This command lists all implemented commands in the MeSH shell.
This is accomplished by looping through the `builtin_str` array defined in `include/mesh.h` and printing it to stdout.

#### shutdown

Usage: `shutdown`

This command logs out the user and then shuts down the MeSH shell.
It does not shut down the Arty Z7, it simply breaks out of the command loop found in `common/mesh.c:mesh_loop()`.

#### login

While not a command per say, MeSH prompts for a user and pin upon boot.
The provided username and pin are compared to values in the generated `mesh_users.h` file to ensure that a valid username and password were entered.
We hash the pins using Libsodium’s crypto_pwhash during the provisioning process and store the hashes inside a generated header. We use the Argon2id algorithm for the hashing instead of the legacy scrypt because of the ability to tune memory consumption and CPU usage separately. We maximized memory hardness so as to minimize the possible parallelization of a password cracking process.

#### logout

Usage: `logout`

This command logs out the user and brings you back to the login prompt, allowing another user to log in.
This is done by clearing the user struct with 0's, exiting the command loop, and calling the `mesh_login` function again.

#### list

Usage: `list`

The `list` command lists all games installed for the currently logged in user.
This command reads the MeSH installed games table row by row until it reaches the end of table flag.
Each game is printed where the logged in user is equivalent to the name in the `User` struct.
See MeSH Install Table for more details on how the MeSH installed games table is implemented in flash.

#### play

Usage: `play INSTALLED_GAME`

Arguments

	INSTALLED_GAME		The name of an installed game to play.

This command launches the specified game.
To do this, it first reads the specified game into RAM at address `0x1fc00040`.
This is a reserved region in memory where the Linux Kernel expects there to be a game.

Once this is loaded into RAM, it writes the size of the binary in bytes to RAM address `0x1fc00000`.
This is a reserved region in memory where the Linux Kernel expects the size of the game binary to be.

Finally, it boots the Linux Kernel from RAM at address `0x10000000`.
The Linux kernel is loaded into RAM at this address when the Arty Z7 is booted.
This offset is specified in the bif on line 6 that is generated by provisionSystem.py.

Control is then passed to the kernel.

#### query

Usage: `query`

This command queries the ext4 partition of the SD card for all games and prints the name of each to stdout.
This is done using the `mesh_query_ext4` function.
This function is derived from the hush shell `ext4fs_ls` function provided by u-boot.
The `mesh_query_ext4` function is a standalone function that sets the read device to the second partition on the SD card and
then lists each regular file in the root of that partition.

It is assumed that the only regular files in the games partition are actually games.

#### install

Usage: `install GAME_NAME`

Arguments

	GAME_NAME		The name of a game located on the games partition of the
					SD card to install.

This command installs the specified game name for the current user.
The game must be in the games partition on the SD card.

This command is implemented very similarly to the `list` command by looping through each row in the game table.
However, when either the uninstalled game flag (`0x00`) or the end of table flag (`0xff`) is located, it writes the table row struct to that location in memory.

If the game is at the end of the table, then it updates the next row to have the end of table flag (`0xff`).

See MeSH Install Table for more details on how the MeSH installed games table is implemented in flash.

We encrypt the signed game binary using Libsodium’s crypto_secretbox API. This internally uses a slightly modified version of Salsa20 for encryption combined with Poly1305 for message authentication. Salsa20/ChaCha20 use an Add-Rotate-Xor (ARX) construction which can be implemented/compiled into code that does not have secret-dependent branch points. The encryption takes a nonce and a key. We store the key in the secrets.h header and prepend the nonce to the encrypted game, since the nonce does not have to be secret. We use the default randomly generated nonces, since research indicates that VM randomness sources are slower but have the same quality as a host VM source.

#### uninstall

Usage: `uninstall GAME_NAME`

Arguments

	GAME_NAME		The name of an installed game to uninstall.

This command uninstalls the specified game name for the logged in user.
The game must be in the games partition on the SD card.

This command is implemented very similarly to the `list` command by looping through each row in the game table.
If the installed game flag is found and the game name and user match, it clears that row to all 1's and sets the flag to `0x00`, signifying an uninstalled game.

See MeSH Install Table for more details on how the MeSH installed games table is implemented in flash.

#### dump

Usage: `dump offset N`

Arguments
	N 			The number of bytes to print to stdout (in hex)
	offset		The offset to start reading from (in hex).

This command is provided for purposes of testing and is not required for the shell.
It provides an easy way to view data written to flash.
This function utilizes u-boot shell commands, however, due to these commands being static functions, it prepares the arguments as a string (to mimic being called from the command line). It is also important to note that this command interprets `offset` and `N` as hexadecimal numbers. 

#### resetflash

Usage: `resetflash`

This command is provided for purposes of testing and is not required for the shell.

It provides an easy way to reset the entire flash. Under the covers, it runs sf erase 0 0x1000000 which erases the first 0x1000000 bytes. The flash is 16 MB == 16MB * 1024KB/MB * 1024B/KB == 16777216B == 0x1000000 bytes.

It is important to note that when erasing flash, you can only clear it by pages. A page is 64KB, therefore you must clear at least 0x10000 bytes at a time. Furthermore, the sf erase function only works on page boundaries and will give you an error if done with an offset at any other point.

### Device Tree

U-Boot is responsible for loading the game binary into a reserved region in RAM. The reference design reserves a memory region by adding the following node to the device tree:

_Arty-Z7-10/project-spec/meta-user/recipes-bsp/device-tree/files/system-user.dts:_

	...
	memory {
		device_type = "memory";
		reg = <0x00000000 0x1fc00000>;
	};

	reserved_memory {
		device_type = "reserved_memory";
		reg = <0x1fc00000 0x00400000>;
	};
	...

### Hardware

To enhance the system’s entropy source, we utilized an open-source TRNG implementation. We also unmapped unnecessary pins of in order to decrease the attack surface of our system.

### Operating System

In order to decrease the attack surface of our system and introduce additional layers of security at the operating system level, we made the following changes to the PetaLinux configuration:

	/Arty-Z7-10 
		- Second Stage Bootloader (U-Boot) 
			~ disabled some network support
		- Kernel 
			~ disabled some network & hardware support 
		- FileSystem 
			~ modified configuration 


### Linux

Linux is responsible for reading the reserved memory region and launching the game. A PetaLinux init app is used to load and launch the game and can be found here:

_Arty-Z7-10/project-spec/meta-user/recipes-apps/mesh-game-loader_


### Building U-Boot Using _make_

The process below builds the u-boot image using make, then copies it into the images folder that petalinux-build uses for the compiled images. It then skips the provisionSystem script and JUST runs deploySystem so that this new build image will be deployed to the SD card.

1. Add the device tree compiler (`dtc`) to your path: `export PATH=$PATH:/home/vagrant/MES/Arty-Z7-10/build/tmp/sysroots/x86_64-linux/usr/bin/`
2. Change directory to u-boot: `cd /home/vagrant/MES/Arty-Z7-10/components/ext_sources/u-boot-ectf` 
3. Run the make command with some environment variables set: `ARCH=arm CROSS_COMPILE=/opt/pkg/petalinux/tools/linux-i386/gcc-arm-linux-gnueabi/bin/arm-linux-gnueabihf- make zynq_ectf_defconfig && ARCH=arm CROSS_COMPILE=/opt/pkg/petalinux/tools/linux-i386/gcc-arm-linux-gnueabi/bin/arm-linux-gnueabihf- make`
4. Copy the build image to the PetaLinux images folder: `cp ./u-boot /home/vagrant/MES/Arty-Z7-10/images/linux/u-boot.elf`
5. Build `MES.bin` using `packageSystem.py` and deploy using `deploySystem.py`

### Cleaning the U-Boot Directory

After using `make`, you will be unable to build with `petalinux-build` until you clean out the directory.
After you complete the following steps, you should be able to build using `petalinux-build` again.
If for some reason you can't, clean the repo and you should be good, so use good version control practices.
Just make sure you don't commit any of the files make creates!

1. Change directory to u-boot. `cd /home/vagrant/MES/Arty-Z7-10/components/ext_sources/u-boot-ectf`
2. Run the `make` command for `mrproper`. This will get rid of all the make files that were created in the directory. `make mrproper`

## 5. References

MITRE provided reference implementation:
https://github.com/mitre-cyber-academy/2019-ectf-insecure-example

XAPP1175: Secure Boot of Zynq-7000 All Programmable SoC:
https://www.xilinx.com/support/documentation/application_notes/xapp1175_zynq_secure_boot.pdf

UG821: Zynq-7000 All Programmable SoC Software Developers Guide:
https://www.xilinx.com/support/documentation/user_guides/ug821-zynq-7000-swdev.pdf

UG1144: PetaLinux Tools Documentation Reference Guide:
https://www.xilinx.com/support/documentation/sw_manuals/xilinx2017_1/ug1144-petalinux-tools-reference-guide.pdf

UG1156: PetaLinux Tools Documentation Workflow Tutorial:
https://www.xilinx.com/support/documentation/sw_manuals/xilinx2017_3/ug1156-petalinux-tools-workflow-tutorial.pdf
