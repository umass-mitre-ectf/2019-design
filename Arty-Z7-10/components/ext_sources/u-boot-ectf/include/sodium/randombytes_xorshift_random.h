
#ifndef randombytes_xorshift_random_H
#define randombytes_xorshift_random_H

#include "export.h"
#include "randombytes.h"

#ifdef __cplusplus
extern "C" {
#endif

SODIUM_EXPORT
extern struct randombytes_implementation randombytes_xoroshift;

#ifdef __cplusplus
}
#endif

#endif
