#include <compiler.h>

#include "core.h"
#include "private/common.h"
#include "crypto_stream_chacha20.h"
#include "utils.h"
#include "randombytes.h"

// Use random keys as initial basis for random gen
#include "secrets.h"

static unsigned char randombuf[32] = {0};

static uint32_t counter = 0x8a8a8a8a;
static short is_init=0;

/* LFSR based on https://www.schneier.com/academic/archives/1994/09/pseudo-random_sequen.html
 * Just need something with long period that isn't a simple increment
 * Polynomial is x^32+x^7+x^6+x^4+x^2+x+1
 *
 * get_ticks has very little entropy but ensures differences between reboots
 * LFSR affects entire state but is deterministic
 * 
 * ChaCha20 that has avalanche property used to get actual sequence
 * So it will smooth over statistical irregularies
 * That arise from the limited entropy of the stirring function
 */
static void randombytes_time_salsa20_stir(void) {
    // Prevent same sequence from being generated every boot
    uint64_t tick_counter=get_ticks();
    counter^=(tick_counter&0x00000000ffffffff);
    counter^=(tick_counter>>32);
    // A more effective stir that uses fixed LFSR
    for(size_t i=0;i<32;i++) {
        randombuf[i]+=((counter>>i)&0xff);
    }
    // Update counter based on one LFSR cycle
    counter = ((((counter >> 31)  /*Shift the 32nd bit to the first
                                      bit*/
               ^ (counter >> 6)    /*XOR it with the seventh bit*/
               ^ (counter >> 4)    /*XOR it with the fifth bit*/
               ^ (counter >> 2)    /*XOR it with the third bit*/
               ^ (counter >> 1)    /*XOR it with the second bit*/
               ^ counter)          /*and XOR it with the first bit.*/
               & 0x0000001)         /*Strip all the other bits off and*/
               <<31)                /*move it back to the 32nd bit.*/
               | (counter >> 1);   /*Or with the counter shifted
                                      right.*/
}

static const char *
randombytes_time_salsa20_implementation_name(void)
{
    return "time_salsa20";
}

static void randombytes_time_salsa20_init_if_needed(void)
{
    if (is_init!=0) {
        return;
    }
    is_init=1;
    // Mix in the random secret keys
    // The pubkey will have a mathematical form but xor never decreases entropy
    for (size_t i=0;i<32;i++) {
        randombuf[i]^=mesh_game_hash_sk[i];
        randombuf[i]^=mesh_game_encrypt_sk[i];
        randombuf[i]^=mesh_game_sign_pk[i];
    }
    randombytes_time_salsa20_stir();
}

static void
randombytes_time_salsa20_random_buf(void * const buf, const size_t size) {
    randombytes_time_salsa20_init_if_needed();
    // Copied from randombytes_buf_deterministic
    // Take advantage of the fact that key size equals seed bytes
    static const unsigned char nonce[crypto_stream_chacha20_ietf_NONCEBYTES] = {
        'L', 'i', 'b', 's', 'o', 'd', 'i', 'u', 'm', 'D', 'R', 'G'
    };

    COMPILER_ASSERT(randombytes_SEEDBYTES == crypto_stream_chacha20_ietf_KEYBYTES);
#if SIZE_MAX > 0x4000000000ULL
    COMPILER_ASSERT(randombytes_BYTES_MAX <= 0x4000000000ULL);
    if (size > 0x4000000000ULL) {
        sodium_misuse();
    }
#endif
    randombytes_time_salsa20_stir();
    crypto_stream_chacha20_ietf((unsigned char *) buf, (unsigned long long) size,
                                nonce, randombuf);
}

static uint32_t
randombytes_time_salsa20_random(void)
{
    uint32_t retval;
    unsigned char * randbuf = (unsigned char*) malloc(sizeof(uint32_t));
    randombytes_time_salsa20_random_buf(randbuf,sizeof(uint32_t));
    memcpy(&retval,randbuf,sizeof(uint32_t));
    free(randbuf);
    return retval;
}

struct randombytes_implementation randombytes_time_salsa20 = {
    SODIUM_C99(.implementation_name =) randombytes_time_salsa20_implementation_name,
    SODIUM_C99(.random =) randombytes_time_salsa20_random,
    SODIUM_C99(.stir =) randombytes_time_salsa20_stir,
    SODIUM_C99(.uniform =) NULL,
    SODIUM_C99(.buf =) randombytes_time_salsa20_random_buf,
    SODIUM_C99(.close =) NULL
};
/*typedef struct randombytes_implementation {
    const char *(*implementation_name)(void);
    uint32_t    (*random)(void);
    void        (*stir)(void);
    uint32_t    (*uniform)(const uint32_t upper_bound);
    void        (*buf)(void * const buf, const size_t size);
    int         (*close)(void);
} randombytes_implementation;*/
