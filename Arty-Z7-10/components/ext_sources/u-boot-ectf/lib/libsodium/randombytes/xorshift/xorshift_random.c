#include <compiler.h>

#include "core.h"
#include "utils.h"
#include "randombytes.h"
/*
WARNING: This is a deterministic testing-only randomness implementation!
Same values will be produced across startup!
Make sure to replace me!
*/

#warning This is a deterministic testing-only randomness implementation!

// http://vigna.di.unimi.it/xorshift/xoroshiro128plus.c

uint64_t s[2] = {0x19283746afbecddc,0x1234567890abcdef};

static inline uint64_t rotl(const uint64_t x, int k) {
        return (x << k) | (x >> (64 - k));
}

static uint64_t next(void) {
        const uint64_t s0 = s[0];
        uint64_t s1 = s[1];
        const uint64_t result = s0 + s1;

        s1 ^= s0;
        s[0] = rotl(s0, 55) ^ s1 ^ (s1 << 14); // a, b
        s[1] = rotl(s1, 36); // c
        return result;
}

// Start libsodium interface
static const char *
randombytes_xorshift_implementation_name(void)
{
    return "xoroshift128";
}

static uint32_t
randombytes_xorshift_random(void)
{
    uint64_t nextnum=next();
    uint32_t retnum = (uint32_t) nextnum;
    retnum ^= (nextnum>>32);
    return retnum;
}

static void
randombytes_xorshift_random_buf(void * const buf, const size_t size) {
    const size_t divfactor = sizeof(uint32_t)/sizeof(unsigned char);
    size_t copyovertimes = size/divfactor;
    size_t copyoverremain = size%divfactor;
    unsigned char * bufptr = (unsigned char *) buf;
    for (size_t i = 0; i < copyovertimes; i++) {
        uint32_t nextval = next();
        bufptr[4*i]=(nextval&0xff000000) >> 24;
        bufptr[4*i+1]=(nextval&0x00ff0000) >> 16;
        bufptr[4*i+2]=(nextval&0x0000ff00) >> 8;
        bufptr[4*i+3]=nextval&0x000000ff;
    }
    for (size_t i = 0; i < copyoverremain; i++) {
        uint8_t shiftover = 8*i;
        uint32_t mask = 0x000000ff << shiftover;
        uint32_t nextval = next();
        bufptr[4*copyovertimes+i]=nextval&mask >> shiftover;
    }
}

struct randombytes_implementation randombytes_xoroshift = {
    SODIUM_C99(.implementation_name =) randombytes_xorshift_implementation_name,
    SODIUM_C99(.random =) randombytes_xorshift_random,
    SODIUM_C99(.stir =) NULL,
    SODIUM_C99(.uniform =) NULL,
    SODIUM_C99(.buf =) randombytes_xorshift_random_buf,
    SODIUM_C99(.close =) NULL
};
/*typedef struct randombytes_implementation {
    const char *(*implementation_name)(void);
    uint32_t    (*random)(void);
    void        (*stir)(void);
    uint32_t    (*uniform)(const uint32_t upper_bound);
    void        (*buf)(void * const buf, const size_t size);
    int         (*close)(void);
} randombytes_implementation;*/
